from crate import client
from Const import Constants
import OutputFileConversion

class DbConnection:
    'Common base class for connection and reading data from crate db'
    
    __dbHost = ""
    
    def __init__(self, dbHost):
        self.__dbHost = dbHost
        self.connection = client.connect(self.__dbHost)
        self.cursor = self.connection.cursor()
    
    def parseChannels(self, dbVarName, conditions, returnvalues, dict, channelName, value):
        dbVarName = "d['" +channelName.upper().replace(".","_") + "']"
        if len(value) > 1:
            conditions = value[1]
            dict[dbVarName] = [value[1][1], value[1][0]]
        returnvalues += "," + dbVarName
        return returnvalues
    
    #Give limit zero to reterive all the results
    def query(self, tablename, dict, type, channels, parentpath,limit = 100):
        queries = []
        vehicles = []
        returnvalues = ""
        dbVarName = ""
        conditions = ""
        values = ""
        if len(channels) > 0:
            returnvalues += "c,t,m,v"
            for key, value in channels.items():
                print(value[0])
                if value[0] in Constants.SUBCHANNELS:
                    for subChannel in Constants.SUBCHANNELS[value[0]]:
                        returnvalues += self.parseChannels(subChannel, conditions, values, dict, subChannel, value)
                else:
                    returnvalues += self.parseChannels(dbVarName, conditions, values, dict, value[0], value)
            
        else:
            returnvalues = "c,t,m,v,d"


        query = "select " + returnvalues+ " from "+ tablename
        count = 0;
        start_time = 0
        end_time = 0
        
        
        if len(dict) > 0:
            query += " where"
        first = True
        vehicles = []
        for key, value in dict.items():
            if key == Constants.CONST_T:
                start_time = value[0]
            
            if key == Constants.CONST_ET:
                key = Constants.CONST_T
                end_time = value[0]
                 
            if (key == Constants.CONT_V):
                firstVehicle = True
                vehicles = value[0].split(",")
                if first:
                    query += " ("
                else:
                    query += " AND ("
                for vehicle in vehicles:
                    if firstVehicle:
                        query +=  " " + str(key) + " " + str(value[1]) + " " + str(vehicle)
                        firstVehicle = False
                    else:
                        query +=" OR " + str(key) + " " + str(value[1]) + " " + str(vehicle)
                    continue
                query += ")"
            else: 
                if first:
                    query +=  " " + str(key) + " " + str(value[1]) + " " + str(value[0])
                    first = False
                else:
                    query +=  " AND " + str(key) + " " + str(value[1]) + " " + str(value[0])
        
        
        
        if limit >= 1:
            query += " LIMIT " + str(limit)
        #else:
        #    query += " LIMIT " + "100000"
        print (query)
        
        if len(queries) > 1:
            for separateQuery in queries:
                print (separateQuery)
                self.cursor.execute(separateQuery)
                
                fileConversion = OutputFileConversion.OutputFileConversion(type)
            
                fileConversion.convertDBOutput(self.cursor.description, self.cursor.fetchall(), start_time, end_time, parentpath, channels)
        else:
            self.cursor.execute(query)
            
            fileConversion = OutputFileConversion.OutputFileConversion(type)
            
            fileConversion.convertDBOutput(self.cursor.description, self.cursor.fetchall(), start_time, end_time, parentpath, channels)
        return
    
    
    
    
    
    #Calculations for the missing trips
    def querytrips(self, tablename, vid, timestart, timeend):
        query = "SELECT * from cl_prod_data where d['OBDII_SPEED'] != 0 AND d['GPS_SURFACE_KINETIC_SPEED']!= 0 AND v=73  LIMIT 10"
        #query += " where v =" + str(str(vid)) + " AND t >=" + str(timestart) + " AND t <= " + str(timeend) + " AND d['GPS_SURFACE_KINETIC_SPEED'] != 0"
        
        self.cursor.execute(query)
        
        fileConversion = OutputFileConversion.OutputFileConversion(type)
        
        result  =  self.cursor.fetchall()
        return result
    
    
