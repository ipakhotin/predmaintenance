
%get data
readAllData2

%calc TPS variance

%disp('data unpacked - calculating maf ratio')

tpsvarinterval = 40;

for i=1:length(tps)

if(i>tpsvarinterval/2 && i<(length(tps)-tpsvarinterval/2))
tpsvar(i) = var(tps(i-tpsvarinterval/2:i+tpsvarinterval/2));
end

end

%flag periods where TPS is stable

tpsvar2 = tpsvar;
tpsvar2(tpsvar2>0) = 1;
idxdash = find(tpsvar2==0);
idxdash2 = find(tps==min(tps));
idx = intersect(idxdash,idxdash2);

%calc MAF variance

mafvarinterval = 40;

for i=1:length(maf)

if(i>mafvarinterval/2 && i<(length(tps)-mafvarinterval/2))
mafvar(i) = var(maf(i-mafvarinterval/2:i+mafvarinterval/2));
end

end

%flag periods where MAF is stable

mafvar2 = mafvar;
idx2 = find(mafvar2<0.002);

idx3 = intersect(idx,idx2);

mafratio = maf(idx3)./(tps(idx3).*rpm(idx3));

clear idx idx2 idx3

%output to file

filename = 'airFilterOutput.txt';
fid = fopen(filename,'a');
%outstr = [date1 ',' num2str(mean(mafratio)) ',' date2 ',' num2str(median(mafratio)) ',' num2str(var(mafratio))];
outstr = [date1 ',' num2str(mean(mafratio)) ',' date2 ',' num2str(median(mafratio)) ',' num2str(var(mafratio))];
fprintf(fid,"%s\n",outstr);
fclose(fid);
%}