import sys
import os
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
import glob

files = None

if(len(sys.argv) < 2):
	print("No vid specified")
	exit(1)
else:
	vid = sys.argv[1]

if(len(sys.argv) == 3):
	files = sys.argv[2].replace("'", "").replace("[", "").replace("]", "").split(",")
	for index in range(len(files)):
		files[index] = files[index].strip()

def get_min_tps(tps_list):
	only_tps_value = [x[1] for x in tps_list if x[1] != 0.0]
	return min(only_tps_value)

def get_max_tps(tps_list):
	only_tps_value = [x[1] for x in tps_list if x[1] != 0.0]
	return max(only_tps_value)

def calc_other_braking_factor(combined_list, min_tps):
	#avg deceleration when tps is not min
	#should include all other sources of braking e.g. engine brake, drag, inertia
	count = 0
	ret = 0.0
	prev_ts = 0
	prev_speed = 0
	for combined_index in range(len(combined_list)):
		if prev_ts == 0:
			prev_ts = combined_list[combined_index][0]
			prev_speed = combined_list[combined_index][1]
			continue

		curr_speed = combined_list[combined_index][1]
		curr_ts = combined_list[combined_index][0]
		# print(curr_ts)
		# print(prev_ts)
		acceleration = ((curr_speed / 3.6)** 2 - (prev_speed / 3.6) ** 2) / ((curr_ts - prev_ts)/1000)

		if acceleration < 0 and min_tps < combined_list[combined_index][2]:
			ret += acceleration
			# print("other: " + str(acceleration))
			count += 1

		prev_ts = combined_list[combined_index][0]
		prev_speed = combined_list[combined_index][1]

	print("other count: " + str(count))
	return ret

def calc_brake_factor(combined_list, min_tps):
	#avg deceleration when tps *is* min
	count = 0
	ret = 0.0
	prev_ts = 0
	prev_speed = 0
	for combined_index in range(len(combined_list)):
		if prev_ts == 0:
			prev_ts = combined_list[combined_index][0]
			prev_speed = combined_list[combined_index][1]
			continue

		curr_speed = combined_list[combined_index][1]
		curr_ts = combined_list[combined_index][0]
		# print(curr_ts)
		# print(prev_ts)
		acceleration = ((curr_speed / 3.6)** 2 - (prev_speed / 3.6) ** 2) / ((curr_ts - prev_ts)/1000)

		if acceleration < 0 and min_tps >= combined_list[combined_index][2]:
			ret += acceleration
			# print("brake: " + str(acceleration))
			# print(combined_list[combined_index][0])
			count += 1

		prev_ts = combined_list[combined_index][0]
		prev_speed = combined_list[combined_index][1]
	
	print("brake count: " + str(count))

	return ret

def calc_wear(speed_list, tps_list, combined_list):
	result = 0.0
	min_tps = get_min_tps(tps_list)
	print("min: " + str(min_tps))
	max_tps = get_max_tps(tps_list)
	threshold = min_tps + (max_tps - min_tps) * 7/100
	other_braking_factor = calc_other_braking_factor(combined_list, threshold)
	# other_braking_factor = 0
	return (calc_brake_factor(combined_list, threshold) - other_braking_factor)


speed_list = []
tps_list = []

if files is None:
	speed_dir = os.path.join(SCRIPT_DIR,"..", "..", "..", "extractedData", "Combined", "OBDII.SPEED")
	tps_dir = os.path.join(SCRIPT_DIR,"..", "..", "..", "extractedData", "Combined", "OBDII.TPS")

	for filename in glob.glob(os.path.join(speed_dir, vid + "*")):
		with open(filename, 'r') as f:
			f.readline()
			for line in f:
				data = line.strip().split(",")
				speed_list.append((int(data[1]), float(data[4]))) #timestamp, speed


	for filename in glob.glob(os.path.join(tps_dir, vid + "*")):
		with open(filename, 'r') as f:
			f.readline()
			for line in f:
				data = line.strip().split(",")
				tps_list.append((int(data[1]), float(data[4]))) #timestamp, tps
else:
	for filepath in files:
		if "OBDII.SPEED" in filepath:
			speed_file = os.path.join(SCRIPT_DIR,"..", "..", "..", filepath)
			with open(speed_file, 'r') as f:
				f.readline()
				for line in f:
					data = line.strip().split(",")
					speed_list.append((int(data[1]), float(data[4]))) #timestamp, speed
		elif "OBDII.TPS" in filepath:
			tps_file = os.path.join(SCRIPT_DIR,"..", "..", "..", filepath)
			with open(tps_file, 'r') as f:
				f.readline()
				for line in f:
					data = line.strip().split(",")
					tps_list.append((int(data[1]), float(data[4]))) #timestamp, tps

combined_data = [] #ts, speed, tps
prev_ts = 0
for speed_index in range(len(speed_list)):
	if speed_list[speed_index][0] == tps_list[speed_index][0] and speed_list[speed_index][0] != prev_ts:
		combined_data.append((speed_list[speed_index][0], speed_list[speed_index][1], tps_list[speed_index][1]))

	prev_ts = speed_list[speed_index][0]

min_tps = get_min_tps(tps_list)
print("min tps len: " + str(len([x for x in combined_data if x[2] == min_tps])))

brake_wear = calc_wear(speed_list, tps_list, combined_data)

print("brake wear: " + str(brake_wear))

f = open(os.path.join(SCRIPT_DIR, "..", "..", "..", "output", "brakes", "output.csv"), "a")
f.write(str(speed_list[0][0]) + "," + str(speed_list[-1][0]) + "," + str(vid) + "," + "brakes" + "," + str(brake_wear) + "\n")
