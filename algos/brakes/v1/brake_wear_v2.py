import sys
import os
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
import glob
import math
from scipy.interpolate import interp1d

#testing - IP

spd = []#list to hold interpolated speed values-IP

#output speed and accel to file for testing-IP
#f = open('accelOutput','w')
#f.close()

if(len(sys.argv) < 2):
	print("No vid specified")
	exit(1)
else:
	vid = sys.argv[1]

def get_min_tps(tps_list):
	only_tps_value = [x[1] for x in tps_list if x[1] != 0.0]
	return min(only_tps_value)

def get_max_tps(tps_list):
	only_tps_value = [x[1] for x in tps_list if x[1] != 0.0]
	return max(only_tps_value)

def calc_other_braking_factor(combined_list, min_tps):
	#avg deceleration when tps is not min
	#should include all other sources of braking e.g. engine brake, drag, inertia
	count = 0
	ret = 0.0
	prev_ts = 0
	prev_speed = 0

	#interpolate between speed measurements-IP
	#spd = array('d')
	
	idx = []
	interpSegment = []
	segmentBegin = 0.0
	segmentEnd = 0.0
	segmentBeginIdx = 0
	segmentEndIdx = 0
	lengthOfsegment = 0

	#flag nans (data points in between speed measurements) and create array with non-nan values
	for combined_index in range(len(combined_list)):
		if combined_index>1 and combined_list[combined_index][1]>0 and combined_list[combined_index][1]-combined_list[combined_index-1][1] == 0:
			spd.insert(combined_index,'nan')
		else:
			spd.insert(combined_index,combined_list[combined_index][1])
			idx.append(combined_index)

	#interpolate across nans
	for i in range(len(idx)):
		if i>1:
			segmentBeginIdx = idx[i-1]
			segmentEndIdx = idx[i]
			segmentBegin = spd[segmentBeginIdx]
			segmentEnd = spd[segmentEndIdx]
			lengthOfSegment = idx[i] - idx[i-1]
			if lengthOfSegment > 1:
				interpSegment = interp1d([0,lengthOfSegment],[segmentBegin,segmentEnd])(range(0,lengthOfSegment))
				for j in range(len(interpSegment)):
					spd[idx[i-1]+j] = interpSegment[j]

	#interpolation-end -IP

	for combined_index in range(len(combined_list)):
		
		#print("SPEED" + str(spd[combined_index]))
		if prev_ts == 0:
			prev_ts = combined_list[combined_index][0]
			#prev_speed = combined_list[combined_index][1]
			prev_speed = spd[combined_index]
			continue

		#curr_speed = combined_list[combined_index][1]
		curr_speed = spd[combined_index]
		curr_ts = combined_list[combined_index][0]
		# print(curr_ts)
		# print(prev_ts)
		acceleration = ((curr_speed / 3.6)** 2 - (prev_speed / 3.6) ** 2) / ((curr_ts - prev_ts)/1000)

		#test file to store accel and speed values for later viewing-IP
		#f = open('accelOutput','a')
		#f.write(str(acceleration) + "," + str(curr_speed))
		#f.write('\n')
		#f.close()

		if acceleration < 0 and min_tps < combined_list[combined_index][2]:
			ret += acceleration
			# print("other: " + str(acceleration))
			count += 1

		prev_ts = combined_list[combined_index][0]
		#prev_speed = combined_list[combined_index][1]
		prev_speed = spd[combined_index]

	print("other count: " + str(count))
	return ret

def calc_brake_factor(combined_list, min_tps):
	#avg deceleration when tps *is* min
	count = 0
	ret = 0.0
	prev_ts = 0
	prev_speed = 0
	for combined_index in range(len(combined_list)):
		if prev_ts == 0:
			prev_ts = combined_list[combined_index][0]
			#prev_speed = combined_list[combined_index][1]
			prev_speed = spd[combined_index]
			continue

		#curr_speed = combined_list[combined_index][1]
		curr_speed = spd[combined_index]
		curr_ts = combined_list[combined_index][0]
		# print(curr_ts)
		# print(prev_ts)
		acceleration = ((curr_speed / 3.6)** 2 - (prev_speed / 3.6) ** 2) / ((curr_ts - prev_ts)/1000)
		
		if acceleration < 0 and min_tps >= combined_list[combined_index][2]:
			ret += acceleration
			# print("brake: " + str(acceleration))
			# print(combined_list[combined_index][0])
			count += 1

		prev_ts = combined_list[combined_index][0]
		#prev_speed = combined_list[combined_index][1]
		prev_speed = spd[combined_index]
	
	print("brake count: " + str(count))

	return ret

def calc_wear(speed_list, tps_list, combined_list):
	result = 0.0
	min_tps = get_min_tps(tps_list)
	print("min: " + str(min_tps))
	max_tps = get_max_tps(tps_list)
	threshold = min_tps + (max_tps - min_tps) * 5/100
	other_braking_factor = calc_other_braking_factor(combined_list, threshold)
	# other_braking_factor = 0
	return (calc_brake_factor(combined_list, threshold) - other_braking_factor)


speed_list = []
tps_list = []

speed_dir = os.path.join(SCRIPT_DIR,"..", "..", "..", "extractedData", "Combined", "OBDII.SPEED")
tps_dir = os.path.join(SCRIPT_DIR,"..", "..", "..", "extractedData", "Combined", "OBDII.TPS")

for filename in glob.glob(os.path.join(speed_dir, vid + "*")):
	with open(filename, 'r') as f:
		f.readline()
		for line in f:
			data = line.strip().split(",")
			speed_list.append((int(data[1]), float(data[4]))) #timestamp, speed


for filename in glob.glob(os.path.join(tps_dir, vid + "*")):
	with open(filename, 'r') as f:
		f.readline()
		for line in f:
			data = line.strip().split(",")
			tps_list.append((int(data[1]), float(data[4]))) #timestamp, tps

combined_data = [] #ts, speed, tps
prev_ts = 0
for speed_index in range(len(speed_list)):
	if speed_list[speed_index][0] == tps_list[speed_index][0] and speed_list[speed_index][0] != prev_ts:
		combined_data.append((speed_list[speed_index][0], speed_list[speed_index][1], tps_list[speed_index][1]))

	prev_ts = speed_list[speed_index][0]

min_tps = get_min_tps(tps_list)
print("min tps len: " + str(len([x for x in combined_data if x[2] == min_tps])))

brake_wear = calc_wear(speed_list, tps_list, combined_data)

f = open(os.path.join(SCRIPT_DIR, "..", "..", "..", "output", "brakes", "output.csv"), "a")
f.write(str(speed_list[0][0]) + "," + str(speed_list[-1][0]) + "," + str(vid) + "," + "brakes" + str(brake_wear) + "\n")
