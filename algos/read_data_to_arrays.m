%Reads data from files into cell array variables listed below.
%Variable vid MUST be set
%Timestamps for values are in variables prefixed with 'ts_' (e.g. ts_speed)
%voltage
%speed
%maf

%to prevent Octave from thinking this is a script file
1;

function [ts, value] = readfile(filename)
filename
[a1, a2, a3, a4, a5, a6, a7, a8] = textread(filename, ' %d %f %d %d %f %s %s %s', 'delimiter', ',', 'headerlines', 1);
ts = a2;
value = a5;
endfunction

function [ts, value] = read(directory, vid)
files = glob(strcat(directory, num2str(vid), '*'));
for i = 1:length(files)
	filename = files{i, 1};
	[ts, value] = readfile(filename);
end
endfunction

%Speed
function [ts, value] = getSpeed(vid)
	[ts, value] = read('extractedData/Combined/OBDII.SPEED/', vid);
endfunction

%Voltage
function [ts, value] = getVoltage(vid)
	[ts, value] = read('extractedData/Combined/OBDII.VOLTAGE/', vid);
endfunction

%TPS
function [ts, value] = getTPS(vid)
	[ts, value] = read('extractedData/Combined/OBDII.TPS/', vid);
endfunction

%MAF
function [ts, value] = getMAF(vid)
	[ts, value] = read('extractedData/Combined/OBDII.MAF/', vid);
endfunction

%RPM
function [ts, value] = getRPM(vid)
	[ts, value] = read('extractedData/Combined/OBDII.RPM/', vid);
endfunction