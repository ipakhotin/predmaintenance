class Constants:
    CONST_TABLE_NAME_DB = "cl_prod_data"
    CONST_TABLE_NAME_CAN = "cl_extracted_data"
    CONST_VID = "vid"
    CONST_TIME = "time"
    CONST_END_TIME = "endtime"
    CONT_V = "v"
    CONST_T = "t"
    CONST_ET = "et"
    SUBCHANNELS = {'IMU.FUSION_QPOSE': ['IMU.FUSION_QPOSE.X','IMU.FUSION_QPOSE.Y','IMU.FUSION_QPOSE.Z','IMU.FUSION_QPOSE.S'],
                   "IMU.ACCEL" : ['IMU_ACCEL_X', 'IMU_ACCEL_Y', 'IMU_ACCEL_Z'],'IMU.GYRO' : ['IMU_GYRO_X','IMU_GYRO_Y','IMU_GYRO_Z']}
    
    CHANNELTYPES = {'OBDII.VOLTAGE': "FLOAT_ARRAY","IMU.TEMP" : "FLOAT_ARRAY",
                    "IMU.FUSION_QPOSE" : "FLOAT_ARRAY", "OBDII.MAF" : "FLOAT_ARRAY",
                    "OBDII.TPS" : "FLOAT_ARRAY", "OBDII.RPM" : "FLOAT_ARRAY", "OBDII.SPEED" : "FLOAT_ARRAY",
                    "IMU.ACCEL" : "FLOAT_ARRAY", "OBDII.TEMP" : "FLOAT_ARRAY",'IMU.GYRO': "FLOAT_ARRAY"}
    