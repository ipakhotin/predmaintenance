
%get data
%readAllData2

%read data from Crate

addpath "algos"
vid = argv(){1};
read_data_to_arrays;


[tsmaf, maf] = getMAF(vid);
[tsrpm, rpm] = getRPM(vid);
[tsspeed, speed] = getSpeed(vid);
[tstps, tps] = getTPS(vid);

%calc TPS variance

%disp('data unpacked - calculating maf ratio')

tpsvarinterval = 40;

for i=1:length(tps)

if(i>tpsvarinterval/2 && i<(length(tps)-tpsvarinterval/2))
tpsvar(i) = var(tps(i-tpsvarinterval/2:i+tpsvarinterval/2));
end

end

%flag periods where TPS is stable

tpsvar2 = tpsvar;
tpsvar2(tpsvar2>0) = 1;
idxdash = find(tpsvar2==0);
idxdash2 = find(tps==min(tps));
idx = intersect(idxdash,idxdash2);

%calc MAF variance

mafvarinterval = 40;

for i=1:length(maf)

if(i>mafvarinterval/2 && i<(length(tps)-mafvarinterval/2))
mafvar(i) = var(maf(i-mafvarinterval/2:i+mafvarinterval/2));
end

end

%flag periods where MAF is stable

mafvar2 = mafvar;
idx2 = find(mafvar2<0.002);

idx3 = intersect(idx,idx2);

mafratio = maf(idx3)./(tps(idx3).*rpm(idx3));

clear idx idx2 idx3

%alternate method - flag beginning and end of each throttle period

if(tps(1)==min(tps))
firstindex = 1;
else
tmp = find(tps==min(tps));
firstindex = tmp(1);
clear tmp
end

beginidx = NaN;endidx = NaN;

for i=firstindex:length(tps)
if(i>1 && tps(i)>min(tps) && tps(i-1)==min(tps))
beginidx(end+1) = i;
end

if(i<length(tps) && tps(i)>min(tps) && tps(i+1)==min(tps))
endidx(end+1) = i;
end

end

beginidx(1) = [];endidx(1) = [];

%POSSIBLE IMPROVEMENT - discard really long throttle periods (since they will presumably be more noisy)
%{
maxLength = 10;

for i=1:length(beginidx)
if((endidx(i)-beginidx(i))>maxLength)
beginidx(i) = NaN;endidx(i) = NaN;
end
end

beginidx(isnan(beginidx)==1) = [];
endidx(isnan(endidx)==1) = [];
%}

%then see the delta v for that throttle cycle

for i=1:length(beginidx)
sumtps(i) = sum(tps(beginidx(i):endidx(i)));
deltav(i) = speed(endidx(i))-speed(beginidx(i));
end

%sort by sum TPS to get the maxima

clear sumtpsarray idx idx2 idx3 maxtps maxdeltav
sumtpsarrayinterval = 50;
sumtpsarray = [0:sumtpsarrayinterval:6000];

for i=1:length(sumtpsarray)
idx1 = find(sumtps>(sumtpsarray(i)-sumtpsarrayinterval/2));
idx2 = find(sumtps<(sumtpsarray(i)+sumtpsarrayinterval/2));
idx3 = intersect(idx1,idx2);
if(isempty(idx3)==0)
maxtps(i) = max(sumtps(idx3));
maxdeltav(i) = max(deltav(idx3));
else
maxtps(i) = NaN;
maxdeltav(i) = NaN;
end
clear idx idx2 idx3
end

%run ordinary least squares linear regression for the well-behaved part of the scatter plot

clear maxtps2 maxdeltav2 Xt Yt x N K bet ut

idx = find(maxtps>1500);

maxtps2 = maxtps(1:idx(1));
maxdeltav2 = maxdeltav(1:idx(1));
clear idx

maxtps2(isnan(maxtps2)==1) = [];
maxdeltav2(isnan(maxdeltav2)==1) = [];

Xt = maxtps2;
Yt = maxdeltav2;

X = [ones(length(Xt),1), Xt'];
[N,K]=size(X);
bet = X\Yt(:);
ut = Yt' - X*bet;

%calc mean value for output

meanval = mean(X*bet);

%output to a file

format_output;

write(tstps(1), tstps(end), vid, 'air_filter', num2str(meanval));

%filename = 'airFilterOutput.txt';
%fid = fopen(filename,'a');
%outstr = [num2str(meanval)];
%fprintf(fid,"%s\n",outstr);
%fclose(fid);

