
function soc = getsoc4(ts,volt,vd)

%calculates SoC for the time series using the following method (detailed in Huyn (2016), State of Health Estimation System for Lead-Acid Car Batteries
%and Kerley (2014), AUTOMOTIVE LEAD-ACID BATTERY STATE-OF-HEALTH MONITORING SYSTEM) : 

%split time series into contiguous segments
%in each segment, find if there is a time period in which the voltage range < 0.1 V. The time period length can be set in the script
%if such time periods exist, use table to get from voltage (divided by 6) to specific gravity
%assume 25 C and get from specific gravity to SoC for SLI battery using another table
%unlike getsoc3.m this script will ALWAYS output a SoC value

%table 1 (corresponding to table 1 in Kerley 2014 thesis) relating terminal voltage to specific gravity
vcell = [1.876 1.907 1.932 1.951 1.965 1.976 1.989 2.004 2.016 2.03 2.045 2.058 2.071 2.083 2.097 2.11 2.122 2.135];
sg = [1.03 1.051 1.068 1.081 1.089 1.104 1.119 1.137 1.152 1.169 1.186 1.202 1.216 1.232 1.248 1.262 1.278 1.293];

%table 2 (corresponding to table 2 in Kerley 2014 thesis) relating specific gravity to SoC for SLI battery at 25 C
slisg = [1.265 1.225 1.190 1.155 1.12];
slisoc = [100 75 50 25 0];

%effective range ~ 2-2.1 V per cell or 12-12.6 V per battery

%for averaged data

idx1 = NaN;
idx2 = NaN;
arrayrange = NaN;

%timeDelay = 0.5;%how long voltage should be stable for (in minutes) before SoC should be taken
timeDelay = 0.1;

for i=1:length(vd)
  %for every 100th data point
  if(rem(i,100)==0)
  %find if there are timestamps timeDelay minutes away from each evaluted data point
    idx = find(ts<(ts(i)-timeDelay*60*1000));
    
    %if they exist, find the range
    if(isempty(idx)==0)
    idx1(end+1) = idx(end);
    idx2(end+1) = i;
    arrayrange(end+1) = range(vd(idx(end):i));
    else
    idx1(end+1) = NaN;
    idx2(end+1) = NaN;
    arrayrange(end+1) = NaN;
    end
    
    %if data exists timeDelay minutes away from the evaluated data point and the voltage range is below the threshold, calc SoC
    if(isempty(idx)==0 && range(vd(idx(end):i))<0.12)
    
    if(vd(i)> 12 && vd(i)<12.6)
    v2(i) = vd(i)/6;%divide by six to get voltage per cell
    sg2(i) = interp1(vcell,sg,v2(i),"linear");
    soc(i) = interp1(slisg,slisoc,sg2(i),"linear");
    end
    %{
    if(vd(i)<=12 && vd(i)>11)
    v2(i) = v2(i-1);
    sg2(i) = sg2(i-1);
    soc(i) = 0;
    end
    %}
    if(vd(i)>=12.6)
    v2(i) = v2(i-1);
    sg2(i) = sg2(i-1);
    soc(i) = 100;
    end
    
    if(vd(i)<=12)
    v2(i) = v2(i-1);
    sg2(i) = sg2(i-1);
    soc(i) = soc(i-1);
    end
    
    else
    %if the conditions are not satisfied, estimate SoC using the information available
    %{
    if(vd(i)<=12)
    soc(i) = 0;
    v2(i) = v2(i-1);
    sg2(i) = sg2(i-1);
    end
    %}
    if(vd(i)>=12.6)
    soc(i) = 100;
    v2(i) = v2(i-1);
    sg2(i) = sg2(i-1);
    else
    v2(i) = v2(i-1);
    sg2(i) = sg2(i-1);
    soc(i) = soc(i-1);
    end
    
    end
    clear idx
    
  else
    if(i>1)
      soc(i) = soc(i-1);
      v2(i) = v2(i-1);
      sg2(i) = sg2(i-1);
    else
      soc(i) = 0;
      v2(i) = 0;
      sg2(i) = 0;
    end

  end
end