from crate import client
from datetime import datetime
import sys
import re

#if len(sys.argv) != 2:
	#print "Usage: python", sys.argv[0], "vid", "DD-MM-YYYY"
#	exit(1)

#vid = sys.argv[1]
vid = '219'
start_ts_as_string = '01-01-2017'
#start_ts_as_string = '01-04-2017'
#date = datetime.strptime(sys.argv[2], '%d-%m-%Y')
date = datetime.strptime(start_ts_as_string, '%d-%m-%Y')
start_ts = int((date - datetime(1970, 1, 1)).total_seconds() * 1000)
end_ts = start_ts + 30 * 1000 * 60 * 60 * 24


time_condition = " t >= " + str(start_ts) + " and t < " + str(end_ts)

#connection = client.connect(["sgc1.clickdrive.io:4200", "sgc2.clickdrive.io:4200"])
connection = client.connect('sgc1.clickdrive.io:4200')

cursor = connection.cursor()

#select_count = "select count(*) from cl_extracted_data where v = " + vid + " and " + time_condition
select_count = "select count(*) from cl_prod_data where v = " + vid + " and " + time_condition

cursor.execute(select_count)
result = cursor.fetchone()
pages = int(result[0] / 10000) + 1

#print "ts,OBDII.VOLTAGE"

variableToGrab = "OBDII_MAF"

#select_voltage = "select t, d['OBDII.VOLTAGE'] from cl_extracted_data where v = " + vid + " and " + time_condition + " order by t limit 10000"
#select_voltage = "select t, d['OBDII_RPM'] from cl_extracted_data where v = " + vid + " and " + time_condition + " order by t limit 10000"
#select_voltage = "select t, d['OBDII_SPEED'] from cl_extracted_data where v = " + vid + " and " + time_condition + " order by t limit 10000"
select_voltage = "select t , d['" + variableToGrab + "'] from cl_prod_data where v = " + vid + " and " + time_condition + " order by t limit 10000"

#car_identifier = "car" + vid + ".txt"
car_identifier = "car" + vid + variableToGrab + "on" + start_ts_as_string + ".txt"

trigger = 0

f = open(car_identifier,'w')

for i in range(pages):
	cursor.execute(select_voltage + " offset " + str(i * 10000))
	result = cursor.fetchall()

	for row in result:
		ts = row[0]
		voltage = row[1]
		#if(voltage<9 and voltage>4):trigger = 1
		#if(voltage is not None and voltage>4 and voltage<9):trigger = 1
		if(voltage is not None):trigger = 1
		print(str(ts) + "," + str(voltage))
		f.write(str(ts) + "," + str(voltage)+'\n')

f.close()

trigger1string = "car " + str(vid) + " on " + start_ts_as_string + " : real data exists!"
trigger0string = "car " + str(vid) + " on " + start_ts_as_string + " : no data detected"

if(trigger==1):print(trigger1string)
if(trigger==0):print(trigger0string)
