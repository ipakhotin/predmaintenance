import csv
import geopy.distance

listData = []

with open('/home/clickdrive/Downloads/data_copy.csv', 'rt') as csvfile:
    tempspamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    coords_1 = ()
    coords_2 = ()
    first_row = True
    spamreader = tempspamreader
    for row in spamreader:
        listData.append(row)
    
spamList = listData
index = 0

for row in spamList:
    minDistance = -1
    previousDistance = 0
    closest_lat = 0
    closest_long = 0
    
    if(row[8] != 'NaN' and row[7] != 'NaN'):
        #print ('OBU_Long:' + row[7])
        coords_1 = (float(row[8]), float(row[7]))
        spamreader_2 = listData
        for rowagain in spamreader_2:
            #print (spamreader_2)
            if(rowagain[0] != ''):
                #print ('IMU_LAT:' + rowagain[13] + ' IMU_LONG:' + rowagain[12])
                coords_2 = (float(rowagain[13]), float(rowagain[12]))
                currentDistance = geopy.distance.vincenty(coords_1, coords_2).km
                
                if minDistance > currentDistance or minDistance == -1:
                    minDistance = currentDistance
                    closest_lat = rowagain[13]
                    closest_lang = rowagain[12]
                    index = rowagain[0]
                    #print ('MinDistance:' + str(minDistance))
     
        print(row[0] + "," + index + "," + str(minDistance) + "," + row[8] + "," + row[7] +","+ closest_lat+","+closest_lang)