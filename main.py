import configparser
import DbConnection
import ChannelsCombiner
import Enumerations
import datetime
import ScriptRunner
from Const import Constants
import sys
import os

from pprint import pprint

const = Constants()
lstChannels = []
lstVehicles = []

if len(sys.argv) <= 1:
    print("Usage: configFileName")
    sys.exit(0)

if not sys.argv[1].endswith('.ini'):
    print("File should be an ini file")
    sys.exit(0)

config = configparser.ConfigParser()
config.read(sys.argv[1])

dirname = os.path.dirname(sys.argv[1])

def configSectionMap(section):
    dict = {}
    options = config.options(section)
    for option in options:
        try:
            dict[option] = config.get(section, option)
            if dict[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict[option] = None
    return dict

def parseTime(time):
    dateandtime = time.split("T")
    time = datetime.datetime(int(dateandtime[0].split("/")[2]), int(dateandtime[0].split("/")[1]), int(dateandtime[0].split("/")[0]), int(dateandtime[1].split("-")[0]), int(dateandtime[1].split("-")[1]), int(dateandtime[1].split("-")[2]))
    return str(int(round(time.timestamp()) * 1000))
    
def mapConditions(conditions):
    dict = {}
    for condition , value in conditions.items():
        
        tuple = value.split(":")
        
        if condition == const.CONST_VID:
            condition = const.CONT_V
            vehicles = tuple[0].split(",")
            for v in vehicles:
                lstVehicles.append(v)
        elif condition == const.CONST_TIME:
            condition = const.CONST_T
            tuple[0] = parseTime(tuple[0])
            
        elif condition == const.CONST_END_TIME:
            condition = const.CONST_ET
            tuple[0] = parseTime(tuple[0])
        
        #if int(tuple[1]) == Enumerations.Conditions.EQUAL:
        #    tuple[1] = "="
        #elif int(tuple[1]) == Enumerations.Conditions.NOTEQUAL:
        #    tuple[1] = "!="
        #elif int(tuple[1]) == Enumerations.Conditions.GREATER:
        #    tuple[1] = ">"
        #elif int(tuple[1]) == Enumerations.Conditions.LESSER:
        #    tuple[1] = "<"
        
        dict[condition] = tuple
    return dict

def mapChannels(channels):
    dict = {}
    data = channels['channel']
    separated_channels = data.split(",")
    count = 0
    for channel in separated_channels:
        channelconditions = channel.split(":")
        if len(channelconditions) > 1:
            tuple = channelconditions[1].split("-")
            
            #if int(tuple[0]) == Enumerations.Conditions.EQUAL:
            #    tuple[0] = "="
            #elif int(tuple[0]) == Enumerations.Conditions.NOTEQUAL:
            #    tuple[0] = "!="
            #elif int(tuple[0]) == Enumerations.Conditions.GREATER:
            #    tuple[0] = ">"
            #elif int(tuple[0]) == Enumerations.Conditions.LESSER:
            #    tuple[0] = "<"
            dict[count] = [channelconditions[0], tuple]
            lstChannels.append(channelconditions[0])
        else:
            dict[count] = [(channelconditions[0])]
            lstChannels.append(channelconditions[0])
        count += 1
    #print(separated_channels)
    return dict

def str2bool(v):
  return v.lower() in ("yes", "true", "t", "1")

dict = mapConditions(configSectionMap("Conditions"))
channels = configSectionMap("OutputChannel")
source = configSectionMap("InputSource")['source']
dbHost = configSectionMap("Settings")['serveraddress']
parentpath = configSectionMap("Settings")['parentdirectory']
channels = mapChannels (channels)
combine = configSectionMap("Combiner")['combine']
updatefromdb = configSectionMap("Combiner")['updatefromdb']
combineVehicles = configSectionMap("Combiner")['combinevehicles']
executeScript = configSectionMap("Combiner")['executescript']
scriptFilename = configSectionMap("ScriptExecutor")['filename']
type = int(configSectionMap("ScriptExecutor")['type'])

if not dirname:
    parentpath = parentpath
else:
    parentpath = dirname + "/" + parentpath
formatedDict = {}
limit = 0


for keys, value in dict.items():
    if(keys != "limit"):
        formatedDict[keys] = value
    else:
        limit = int(value[0])
        


tablename = ""
if int(source) == int(Enumerations.InputType.CAN):
    tablename = const.CONST_TABLE_NAME_CAN
elif int(source) == int(Enumerations.InputType.Clickdrive):
    tablename = const.CONST_TABLE_NAME_CAN
elif int(source) == int(Enumerations.InputType.DB):
    tablename = const.CONST_TABLE_NAME_DB
    

if len(channels) <= 0:
    print ("Can't query without any channels\n")
    sys.exit()
#TODO: Generic output is not done
#dict = {'v':417}

result = []
print(formatedDict)
if str2bool(updatefromdb):
    dbConnection = DbConnection.DbConnection(dbHost)
    result = dbConnection.query(tablename, formatedDict, Enumerations.OutputType.CSV, channels, parentpath, limit)
if str2bool(combine):
    combination = ChannelsCombiner.ChannelsCombiner()
    combination.Combine(formatedDict, lstChannels, lstVehicles, parentpath, str2bool(combineVehicles))
if str2bool(executeScript):
    scriptExecution = ScriptRunner.ScriptRunner()
    scriptExecution.runScript(scriptFilename, formatedDict, lstChannels, lstVehicles, parentpath,type)
    
#pprint (result)



