%SoH estimate based on the method of Huyn (2016): "State of Health Estimation System for Lead-Acid Car Batteries Through Cranking Voltage Monitoring"
%output is dV2 - Vth1 - Vth2 - Vth3 as in the original Huyn (2016) interpretation (Figure 21 in paper)

function [batteryHealth,socout] = getsohHuyn2016funcV2(ts,volt,vd,soc);

%create running average of 4 consecutive datapoints as in the thesis

slopeinterval = 4;

for i=1:length(volt)
if(i<=slopeinterval/2)
vslope(i) = nanmean(volt(1:slopeinterval/2));
end

if(i>slopeinterval/2 && i<(length(volt)-slopeinterval/2))
vslope(i) = nanmean(volt(i-slopeinterval/2:i+slopeinterval/2));
end

if(i>=(length(volt))-slopeinterval/2)
vslope(i) = nanmean(volt(i-slopeinterval/2:end));
end
end

%detect slopes to find cranking events - if diff between 2 consecutive samples (of slopeinterval) > 0.25 V

slopeidx = NaN;

for i=5:length(vslope)

if(sum(vslope(i-4:i)<13)>4 && (vslope(i-1)-vslope(i))>0.25)
slopeidx(end+1) = i;
end

end

slopeidx(1) = [];

%find the beginnings of the slopes and delete consequent indices in the same crank
idxToDelete = NaN;

for i=1:length(slopeidx)
  if(i>1 && slopeidx(i)==slopeidx(i-1)+1)
    idxToDelete(end+1) = i;
  end
end

idxToDelete(1) = [];

slopeidx(idxToDelete) = [];

%delete slopes which are >13 V (i.e. when engine is already on)

slopeidx(volt(slopeidx)>13) = [];

%for each identified slope, investigate whether it is really the local maximum or if there is a deeper local maximum nearby

slopeinterval = 8;

for i=1:length(slopeidx)
  if(slopeidx(i)>slopeinterval)
    tmparray = [slopeidx(i)-floor(slopeinterval/2):slopeidx(i)+floor(slopeinterval/2)];
    realLocalMaximum = find(volt(tmparray)==max(volt(tmparray)));
    slopeidx(i) = tmparray(realLocalMaximum(1));
    clear realLocalMaximum tmparray
  end
end

tmp = slopeidx;
clear slopeidx
slopeidx = unique(tmp);
clear tmp

%pre-valley detection averaging to prevent quantisation from interfering with finding local minima

vdinterval = 4;

for i=1:length(volt)
if(i<=vdinterval/2)
vd(i) = nanmean(volt(1:vdinterval/2));
end

if(i>vdinterval/2 && i<(length(volt)-vdinterval/2))
vd(i) = nanmean(volt(i-vdinterval/2:i+vdinterval/2));
end

if(i>=(length(volt)-vdinterval/2))
vd(i) = nanmean(volt(i-vdinterval/2:end));
end
end

%Valley detection

valleyidx = NaN;
valleySlack = 2.5e-3;%'slack' threshold for valley detection, from paper

for ii=1:length(slopeidx)

  for i=slopeidx(ii):slopeidx(ii)+1000
  %extra condition not explicitly mentioned in paper - ignore valleys when car engine is on (terminal voltage >13 V)
    
    if(i>10 && i<(length(vd)-10) && max(volt(i-2:i+2))<13 && sum(vd(i-5)>vd(i-4:i))>3 && sum(vd(i+5)>vd(i:i+4))>3 && sum(vd(i)<vd(i-5:i-1))>3 && sum(vd(i)<vd(i+1:i+5))>3)
      valleyidx(end+1) = i;
    end
    
    if(i>10 && i<(length(volt)-10) && max(volt(i-2:i+2))<13 && sum(volt(i-5)>volt(i-4:i))>3 && sum(volt(i+5)>volt(i:i+4))>3 && sum(volt(i)<volt(i-5:i-1))>3 && sum(volt(i)<volt(i+1:i+5))>3)
      valleyidx(end+1) = i;
    end
  end
 
end

valleyidx(1) = [];

%remove valleys that are too close to each otherwise

closeValleyInterval = 10;

if(isempty(valleyidx)==0)

  lastvalleyidx = valleyidx(1);

  for i=2:length(valleyidx)
  
  if(isnan(valleyidx(i-1))==0)
    lastvalleyidx = valleyidx(i-1);
  end
  
    if((valleyidx(i)-lastvalleyidx)<closeValleyInterval && isequal(valleyidx(i),lastvalleyidx)==0)
      valleyidx(i) = NaN;
    end
  end
end

valleyidx(isnan(valleyidx)==1) = [];

%for each identified valley, investigate whether it is really the local minimum or if there is a deeper local minimum nearby

for i=1:length(valleyidx)
  if(valleyidx(i)>vdinterval)
    tmparray = [valleyidx(i)-floor(vdinterval/2):valleyidx(i)+floor(vdinterval/2)];
    realLocalMinimum = find(volt(tmparray)==min(volt(tmparray)));
    valleyidx(i) = tmparray(realLocalMinimum(1));
    clear realLocalMinimum tmparray
  end
end

tmp = valleyidx;
clear valleyidx
valleyidx = unique(tmp);
clear tmp

%identify the principal features in each cranking event

slopearray = NaN;
valley1array = NaN;
valley2array = NaN;

slopearrayidx = NaN;
valley1arrayidx = NaN;
valley2arrayidx = NaN;

for i=1:length(slopeidx)

tmpidx = find(valleyidx>slopeidx(i) & valleyidx<(slopeidx(i)+1000));

if(isempty(tmpidx)==0 && length(tmpidx)==1)
slopearray(end+1) = volt(slopeidx(i)-1);
valley1array(end+1) = volt(valleyidx(tmpidx));
valley2array(end+1) = -9999;%flag if 2nd valley does not exist

slopearrayidx(end+1) = (slopeidx(i)-1);
valley1arrayidx(end+1) = (valleyidx(tmpidx));
valley2arrayidx(end+1) = NaN;%flag if 2nd valley does not exist
end

if(isempty(tmpidx)==0 && length(tmpidx)>1)
slopearray(end+1) = volt(slopeidx(i)-1);
valley1array(end+1) = volt(valleyidx(tmpidx(1)));
valley2array(end+1) = volt(valleyidx(tmpidx(2)));

slopearrayidx(end+1) = (slopeidx(i)-1);
valley1arrayidx(end+1) = (valleyidx(tmpidx(1)));
valley2arrayidx(end+1) = (valleyidx(tmpidx(2)));
end

clear tmpidx

end

slopearray(1) = [];valley1array(1) = [];valley2array(1) = [];slopearrayidx(1) = [];valley1arrayidx(1) = [];valley2arrayidx(1) = [];

ocv = slopearray;
dv1 = slopearray - valley1array;

%first threshold Vth1 - linear function (from Figure 16 in paper). gradient and y-intercept calculated 'by eye'

mvth1 = 0.2857;
yinterceptvth1 = -0.4657;

Vth1 = dv1*mvth1 + yinterceptvth1;

%second threshold Vth2 - function of SoC (from figure 18 in paper). gradient and y-intercept calculated 'by eye'

mvth2 = 0.005;
yinterceptvth2 = -0.5;

for i=1:length(slopeidx)
  Vth2(i) = soc(slopeidx(i))*mvth2 + yinterceptvth2;
  socout(i) = soc(slopeidx(i));
end

Vth2(isnan(Vth2)==1) = 0;

%third threshold Vth3 - function of temperature (from figure 20 in paper)
%non-linear function, need to interpolate from picture. will we have temp measurements? for now, will assume 25 deg C

%Vth3 = 0.28;%for 25 degrees (from Figure 20 in thesis)
Vth3 = 0.3;%for 30 degrees (from Figure 20 in thesis)

dv2 = valley2array - valley1array;
dv2(dv2<-500) = NaN;%if the 2nd valley is flagged as non-existent, set dv to zero (might need to re-do this in future)

Vth = Vth1 + Vth2 + Vth3;

%compare Vth with dv

for i=1:length(Vth)

batteryHealth(i) = dv2(i) - Vth(i);

end
