%Calculates State of Health and State of Charge of battery using method of Huyn (2016)
addpath "algos"
addpath "algos/battery"
vid = argv(){1};

read_data_to_arrays;

%Voltage should not contain zeroes, should be filtered out by framework
[ts_voltage, voltage] = getVoltage(vid);



%remove zeros in voltageage data
%zeroidx = find(voltage==0);
%voltage(zeroidx) = NaN;
%ts_voltage(zeroidx) = NaN;
%voltage(isnan(voltage)) = [];
%ts_voltage(isnan(ts_voltage)) = [];

%run moving average - needs to be inside an exception since it can crash if the time series is not long enough

interval = 100;%pick interval over which to average

for i=1:length(voltage)
if(i<=interval/2)
vd(i) = nanmean(voltage(1:interval/2));
end

if(i>interval/2 && i<(length(voltage)-interval/2))
vd(i) = nanmean(voltage(i-interval/2:i+interval/2));
end

if(i>=(length(voltage))-interval/2)
vd(i) = nanmean(voltage(i-interval/2:end));
end
end


%get SoC
soc = getsoc4(ts_voltage,voltage,vd);

%get SoH
[batteryHealth,socout] = getsohHuyn2016funcV2(ts_voltage,voltage,vd,soc);
result = strcat(num2str(batteryHealth(1)), ',', num2str(socout(1)))
disp(strcat('SoH,SoC: ', result));
%output to file
format_output;

write(ts_voltage(1), ts_voltage(end), vid, 'battery', result);