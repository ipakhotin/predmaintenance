import numpy as np
import pandas as pd
from pathlib import Path
import sys

def calculate(x):
   return float(x.split(',')[0]), float(x.split(',')[1]),float(x.split(',')[2])

def findClosestValue(x):
    diff = {'xDiff':0,'yDiff':0,'zDiff':0}
    diff['xDiff'] = (abs(1-x['Gyro.X']))
    diff['yDiff'] = (abs(1-x['Gyro.Y']))
    diff['zDiff'] = (abs(1-x['Gyro.Z']))
    
    if max(diff, key=diff.get) == 'xDiff':
        return x['Gyro.X']
    elif max(diff, key=diff.get) == 'yDiff':
        return x['Gyro.Y']
    elif max(diff, key=diff.get) == 'zDiff':
        return x['Gyro.Z']

def harshDriving(x):
    #calculate kmh/s
    kmhs = 0
    if x['SpeedDif'] == 0:
        return 0
    if x['TimeDif'] == 0:
        return 0
    kmhs = abs(x['SpeedDif'])/(x['TimeDif']/1000)
    return abs(kmhs*0.0283) #return g value

def totalDistance(x):
    prev_distance += prev_distance

def performCalc(vehicle_id, files, outputPath):  
    dfSpeed = pd.read_csv(files[0],usecols=[1,4]).rename(columns={'t': 't', 'Value': 'Speed'})
    dfGyro = pd.read_csv(files[1],usecols=[1,4]).rename(columns={'t': 't', 'Value': 'Gyro'})
    dfAccel = pd.read_csv(files[2],usecols=[1,4]).rename(columns={'t': 't', 'Value': 'Accel'})
    dfRpm = pd.read_csv(files[3],usecols=[1,4]).rename(columns={'t': 't', 'Value': 'RPM'})
    dfGyro['Gyro.X'],dfGyro['Gyro.Y'],dfGyro['Gyro.Z'] = zip(*dfGyro['Gyro'].map(calculate))
    dfAccel['Accel.X'],dfAccel['Accel.Y'],dfAccel['Accel.Z'] = zip(*dfAccel['Accel'].map(calculate))
    dfFinal = dfSpeed.merge(dfGyro,on='t')
    dfFinal = dfFinal.merge(dfAccel,on='t')
    dfFinal = dfFinal.merge(dfRpm,on='t')
    dfFinal.drop(['Gyro','Accel'],axis=1,inplace=True)
    dfFinal.sort_values(by='t',inplace=True)
    dfFinal['Gyro'] = dfFinal.apply(findClosestValue,axis=1)
    dfFinal.drop(['Gyro.X','Gyro.Y','Gyro.Z','Accel.X','Accel.Y','Accel.Z'],axis=1,inplace=True)
    dfFinal = dfFinal[dfFinal['RPM'] > 400]
    dfFinal['TimeDif'] = dfFinal['t'] - dfFinal['t'].shift(1)
    dfFinal.dropna(inplace=True)
    dfFinal['SpeedDif'] = dfFinal['Speed'] - dfFinal['Speed'].shift(1)
    dfFinal['MA'] = dfFinal['Speed'].rolling(window=20).mean()
    dfFinal.dropna(inplace=True)
    dfFinal['Distance'] = dfFinal['Speed'] * (dfFinal['TimeDif'] * 0.000000277778)
    dfFinal['HarshDrivingCount'] = 0
    dfFinal['HarshDriving'] = dfFinal.apply(harshDriving,axis=1)
    dfFinal['GyroChange'] = abs(dfFinal['Gyro'] - dfFinal['Gyro'].shift(1))
    dfFinal.dropna(inplace=True)
    dfFinal['SpeedEffect'] = dfFinal['MA'].apply(lambda x: 1 if x>50 else 0 if x<80 else 2 if x>80 else 3)
    dfFinal = dfFinal[dfFinal['Distance'] < 15]
    dfFinal.drop('HarshDrivingCount',axis=1,inplace=True)
    dfFinal.drop(['Speed','RPM','Gyro','TimeDif','SpeedDif'],axis=1,inplace=True)
    dfFinal['GyroChange'] = dfFinal['GyroChange'].apply(lambda x: 0 if x<0.1 else x)
    dfFinal = dfFinal.groupby((dfFinal.Distance.cumsum() % 100).diff().shift().lt(0).cumsum(), as_index=0).agg('sum')
    totalDistanceTraveled = 4000
    tyresTotalLife = 16000
    X = pd.read_csv('training_set')
    X.drop(['Unnamed: 0','t','Distance','MA'],inplace=True,axis=1)
    y = pd.DataFrame(X['Percentage'], columns=['Percentage'])
    X.drop(['Percentage'],inplace=True,axis=1)
    lr = LogisticRegression()
    lr.fit(X,y)
    predictions = lr.predict(dfFinal.drop(['MA','Distance','t'],axis=1))
    dfFinal['Percentage'] = predictions
    dfFinal.to_csv(outputPath+str(vehicle_id)+'_output.csv')


if len(sys.argv) <= 2:
    print("Usage: SpeedFile,TempFile,RPMFile")
    sys.exit(0)

vehicle_id = int(sys.argv[1])
files = sys.argv[2].split(',')

print(sys.argv[2])
files[0] = files[0].split('[')[1]
files[2] = files[3].split(']')[0]
files[0] = files[0].strip("/").strip("'")
files[1] = files[1].strip("/").strip(" ").strip("'")
files[2] = files[2].strip("/").strip(" ").strip("'")
files[3] = files[3].strip("/").strip(" ").strip("'")

str_path = "output/"
path = Path(str_path)
path.mkdir(parents=True, exist_ok=True)

str_path = "output/radiator/"
path = Path(str_path)
path.mkdir(parents=True, exist_ok=True)

performCalc(vehicle_id, files, str_path)