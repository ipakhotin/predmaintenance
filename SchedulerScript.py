import configparser
import sys
import re
import os
from os import fsync
import fileinput
import time
import subprocess
from datetime import datetime

if len(sys.argv) <= 1:
    print("Usage: configFileName")
    sys.exit(0)

if not sys.argv[1].endswith('.ini'):
    print("File should be an ini file")
    sys.exit(0)

def str2bool(v):
  return v.lower() in ("yes", "true", "t", "1")

dirname = os.path.dirname(sys.argv[1])

config = configparser.ConfigParser()
config.read(sys.argv[1])

def modConfig(File, Variable, Setting):
    """
    Modify Config file variable with new setting
    """
    VarFound = False
    AlreadySet = False
    V=str(Variable)
    S=str(Setting)
    # use quotes if setting has spaces #
    if ' ' in S:
        S = '"%s"' % S

    for line in fileinput.input(File, inplace = 1):
        # process lines that look like config settings #
        if not line.lstrip(' ').startswith('#') and ':' in line:
            _infile_var = str(line.split(':')[0].rstrip(' '))
            _infile_set = str(line.split(':')[1].lstrip(' ').rstrip())
            # only change the first matching occurrence #
            if VarFound == False and _infile_var.rstrip(' ') == V:
                VarFound = True
                # don't change it if it is already set #
                if _infile_set.lstrip(' ') == S:
                    AlreadySet = True
                else:
                    line = "%s: %s\n" % (V, S)

        sys.stdout.write(line)


    # Append the variable if it wasn't found #
    if not VarFound:
        print ("Variable '%s' not found.  Adding it to %s" % (V, File))
        with open(File, "a") as f:
            f.write("%s: %s\n" % (V, S))
    #elif AlreadySet == True:
    #    print ("Variable '%s' unchanged" % (V))
    #else:
    #    print ("Variable '%s' modified to '%s'" % (V, S))

    return
        

def modifySpecificVariables(filename, modifiableVariables):
    cwd = os.getcwd()
    if not dirname:
        return
    for key,value in modifiableVariables.items():
        modConfig(dirname +"/"+filename, key, value )

def configSectionMap(section):
    dict = {}
    options = config.options(section)
    for option in options:
        try:
            dict[option] = config.get(section, option)
            if dict[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict[option] = None
    return dict

scriptFiles = []

def mapConfigFiles(scriptFile):
    files = scriptFile.split("-")
    for file in files:
        if file.endswith(".ini"):
            scriptFiles.append(file)
        else:
            print("Not a valid ini file, will be skipped")

pullData = configSectionMap("General")['pulldata']
runScript = configSectionMap("General")['runscript']
scriptFile = configSectionMap("General")['scriptfiles']
dataPull = configSectionMap("DataPull")['every']
runScriptFile = configSectionMap("RunScript")['every']
dataSpan = configSectionMap("RunScript")['dataspan']
mapConfigFiles(scriptFile)

def translateCondition(condition):
    if condition.endswith('min'):
        return int(condition.split("min")[0]) * 60
    if condition.endswith('h'):
        return int(condition.split("h")[0]) * 60 * 60
    if condition.endswith('d'):
        return int(condition.split("d")[0]) * 60 * 60 * 24
    if condition.endswith('m'):
        return int(condition.split("m")[0]) * 60 * 60 * 24 * 30
    return 0;

def checkCondition(currentTime, lasttime, condition):
    timeToSubtract = translateCondition(condition)
    if timeToSubtract == 0:
        return 0
    
    
    startTime = currentTime - timeToSubtract
    if lasttime == 0:
        return startTime
    if startTime < lasttime:
        return 0

    return startTime
    
print(runScript)
print(dataPull)
print(runScriptFile)
print(dataSpan)
print(scriptFiles)

lastPullTime = 0
lastRunTime = 0
currentTime = time.time()


def runMain(filename):
    try:
        cwd = os.getcwd()
        if not dirname:
            command = "/usr/bin/python3 " + "main.py " +filename
        else:
            command = "/usr/bin/python3 " + dirname +"/main.py " + dirname + "/" +filename
        print(command)
        retcode = subprocess.call(command, shell=True)
        if retcode < 0:
            print("Child was terminated by signal", retcode, file=sys.stderr)
        else:
            print("Child returned", retcode, file=sys.stdout)
    except OSError as e:
        print("Execution failed:", e, file=sys.stderr)

try:
    if not dirname:
        file = open("times.t", "r") 
    else:   
        file = open(dirname+"/times.t", "r") 
    text = file.read()
    lastPullTime = float(text.split("\n")[0])
    lastRunTime = float(text.split("\n")[1])
    file.close()
except FileNotFoundError:
    print("Times file not found initializing")

startTime = 0
for files in scriptFiles:
    print(files)
    
    if str2bool(pullData) and str2bool(runScript):
        modifiableVariables = {"updateFromDB" : "true", "executeScript": "false", "combine": "false"}
        modifySpecificVariables(files, modifiableVariables)
        if(startTime == 0):
            startTime = checkCondition(currentTime, lastPullTime, dataPull)
        
        if startTime != 0:
            startDT=datetime.fromtimestamp(startTime).strftime('%d/%m/%YT%H-%M-%S:>')
            endDT=datetime.fromtimestamp(currentTime).strftime('%d/%m/%YT%H-%M-%S:<')
            modifiableVariables = {"time": startDT , "endTime": endDT}
            modifySpecificVariables(files, modifiableVariables)
            runMain(files)
            lastPullTime = currentTime
        #get last pulled data time
        #run the pull script
        modifiableVariables = {"updateFromDB" : "false", "executeScript": "true", "combine": "true"}
        modifySpecificVariables(files, modifiableVariables)
        if(startTime == 0):
            startTime = checkCondition(currentTime, lastRunTime, runScriptFile)
        if startTime != 0:
            startTime = currentTime - translateCondition(dataSpan)
            startDT=datetime.fromtimestamp(startTime).strftime('%d/%m/%YT%H-%M-%S:>')
            endDT=datetime.fromtimestamp(currentTime).strftime('%d/%m/%YT%H-%M-%S:<')
            modifiableVariables = {"time": startDT , "endTime": endDT}
            modifySpecificVariables(files, modifiableVariables)
            runMain(files)
            lastRunTime = currentTime
        #get last run time also
    elif str2bool(pullData):
        modifiableVariables = {"updateFromDB" : "true", "executeScript": "false", "combine": "false"}
        modifySpecificVariables(files, modifiableVariables)
        if(startTime == 0):
            startTime = checkCondition(currentTime, lastPullTime, dataPull)
        if startTime != 0:
            modifiableVariables = {"time" : str(startTime), "endTime": str(currentTime)}
            modifySpecificVariables(files, modifiableVariables)
            lastPullTime = currentTime
            runMain(files)
    elif str2bool(runScript):
        modifiableVariables = {"updateFromDB" : "false", "executeScript": "true", "combine": "true"}
        modifySpecificVariables(files, modifiableVariables)
        if(startTime == 0):
            startTime = checkCondition(currentTime, lastRunTime, runScriptFile)
        if startTime != 0:
            startTime = currentTime - translateCondition(dataSpan)
            modifiableVariables = {"time" : str(startTime), "endTime": str(currentTime)}
            modifySpecificVariables(files, modifiableVariables)
            lastRunTime = currentTime
            runMain(files)

if not dirname:
    file = open("times.t", "w")
else:
    file = open(dirname+"/times.t", "w")

file.write(str(lastPullTime) + "\n") 
file.write(str(lastRunTime))
file.close()
