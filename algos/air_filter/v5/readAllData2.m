
%read MAF
cd('OBDII.MAF')

filename = ls;

[a1,a2,a3,a4,a5,a6,a7,a8] = textread(filename,' %s %s %s %s %s %s %s %s','delimiter',',','headerlines',1);

tsunsorted = str2num(char(a2));
dataunsorted = str2num(char(a5));

mat1(1,:) = tsunsorted;mat1(2,:) = dataunsorted;

[Y,I]=sort(mat1(1,:));
B=mat1(:,I);

tsmaf = B(1,:);
maf = B(2,:);

cd ..

clear a1 a2 a3 a4 a5 a6 a7 a8 tsunsorted dataunsorted mat1 Y I B filename

%read TPS
cd('OBDII.TPS')

filename = ls;

[a1,a2,a3,a4,a5,a6,a7,a8] = textread(filename,' %s %s %s %s %s %s %s %s','delimiter',',','headerlines',1);

tsunsorted = str2num(char(a2));
dataunsorted = str2num(char(a5));

mat1(1,:) = tsunsorted;mat1(2,:) = dataunsorted;

[Y,I]=sort(mat1(1,:));
B=mat1(:,I);

tstps = B(1,:);
tps = B(2,:);

cd ..

clear a1 a2 a3 a4 a5 a6 a7 a8 tsunsorted dataunsorted mat1 Y I B filename

%read TPS
cd('OBDII.RPM')

filename = ls;

[a1,a2,a3,a4,a5,a6,a7,a8] = textread(filename,' %s %s %s %s %s %s %s %s','delimiter',',','headerlines',1);

tsunsorted = str2num(char(a2));
dataunsorted = str2num(char(a5));

mat1(1,:) = tsunsorted;mat1(2,:) = dataunsorted;

[Y,I]=sort(mat1(1,:));
B=mat1(:,I);

tsrpm = B(1,:);
rpm = B(2,:);

cd ..

clear a1 a2 a3 a4 a5 a6 a7 a8 tsunsorted dataunsorted mat1 Y I B filename

%read Speed
cd('OBDII.Speed')

filename = ls;

[a1,a2,a3,a4,a5,a6,a7,a8] = textread(filename,' %s %s %s %s %s %s %s %s','delimiter',',','headerlines',1);

tsunsorted = str2num(char(a2));
dataunsorted = str2num(char(a5));

mat1(1,:) = tsunsorted;mat1(2,:) = dataunsorted;

[Y,I]=sort(mat1(1,:));
B=mat1(:,I);

tsspeed = B(1,:);
speed = B(2,:);

cd ..

clear a1 a2 a3 a4 a5 a6 a7 a8 tsunsorted dataunsorted mat1 Y I B filename

%read accel
%{
cd('IMU.ACCEL')

filename = ls;

[a1,a2,a3,a4,a5,a6,a7,a8,a9,a10] = textread(filename,' %s %s %s %s %s %s %s %s %s %s','delimiter',',','headerlines',1);

tsunsorted = str2num(char(a2));
strtmp = (char(a5));
accelxunsorted = str2num(strtmp(:,2:end));
clear strtmp
accelyunsorted = str2num(char(a6));
strtmp = char(a7);
accelzunsorted = str2num(strtmp(:,1:7));
clear strtmp
%accelzunsorted = str2num(strtmp(:,1:end-3));
%clear strtmp

mat1(1,:) = tsunsorted;mat1(2,:) = accelxunsorted;mat1(3,:) = accelyunsorted;mat1(4,:) = accelzunsorted;

[Y,I]=sort(mat1(1,:));
B=mat1(:,I);

tsspeed = B(1,:);
accelx = B(2,:);
accely = B(3,:);
accelz = B(4,:);

cd ..

clear a1 a2 a3 a4 a5 a6 a7 a8 tsunsorted accelxunsorted accelyunsorted accelzunsorted mat1 Y I B filename
%}
%infer accel from speedometer


spd = 0;

for i=2:length(speed)

if(speed(i)>0 && (speed(i)-speed(i-1))==0)
spd(i) = NaN;
else
spd(i) = speed(i);
end
end

idx = find(isnan(spd)==0);

for i=2:length(idx)
segment = spd(idx(i-1):idx(i));
lengthOfSegment = length(segment);
if(lengthOfSegment>1)
interpSegment = interp1([1 lengthOfSegment],[segment(1) segment(end)],[1:1:lengthOfSegment]);
spd(idx(i-1):idx(i)) = interpSegment;
end
clear segment lengthOfSegment interpSegment

end

accn(1) = 0;
%{
for i=2:length(spd)
accn(i) = (((spd(i)-spd(i-1))/3.6)/((tsspeed(i)-tsspeed(i-1))/1000))/9.8;
end
%}

interval = 2;

for i=interval+1:length(spd)
accn(i) = (((spd(i)-spd(i-interval))/3.6)/((tsspeed(i)-tsspeed(i-interval))/1000))/9.8;
end


