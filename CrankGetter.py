from crate import client
from Const import Constants
import DbConnection

class CrankGetter(DbConnection.DbConnection):
	'''Class to get only data around cranks'''
	def __init__(self, dbHost):
		super(CrankGetter, self).__init__(dbHost)

	def getCrankTimestamp(self, vid, start_ts, stop_ts):
		query = "select t/10000, min(t), min(d['OBDII_VOLTAGE']) from cl_extracted_data where v = "+ vid + " and d['OBDII_VOLTAGE'] < 9 and d['OBDII_VOLTAGE'] > 4 and t > " + start_ts +" and t < " + stop_ts +" group by t/10000 order by t/10000 limit 100"
		self.cursor.execute(query)
		return self.cursor.fetchall()

	'''Gets data from 20 seconds before the crank to 20 seconds after the crank'''
	def getCrankData(self, dict, type, parentpath):

		for key, value in dict.items():
			if key == Constants.CONST_T:
				start_time = value[0]

			if key == Constants.CONST_ET:
				key = Constants.CONST_T
				end_time = value[0]

			if (key == Constants.CONT_V):
				vehicles = value[0].split(",")

		for vehicle in vehicles:
			result = self.getCrankTimestamp(vehicle, start_time, end_time)

			for crank in result:
				crank_ts = int(crank[1])
				start_ts = str(crank_ts - 20000)
				end_ts = str(crank_ts + 20000)

				info = {}
				info[Constants.CONST_T] = [start_ts, '>']
				info[Constants.CONST_ET] = [end_ts, '<']
				info[Constants.CONT_V] = [vehicle, '=']

				channels = {}
				channels[0] = ['OBDII.VOLTAGE', ['>', '0']]

				self.query("cl_extracted_data", info, type, channels, parentpath, 100000)