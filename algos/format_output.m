%Formats and writes output as csv to file
%Format: start_ts, end_ts, vid, algo/model name, value(s)
%start_ts and end_ts must be passed in as numeric values
%vid, model name and values must be passed in as strings
%multiple values must be separated with commas

%to prevent Octave from thinking this is a script file
1;

function write(start_ts, end_ts, vid, model_name, values)
	filename = strcat(['output/' model_name '/output.csv']);
	fid = fopen(filename,'a');
	outstr = strcat([num2str(start_ts, '%ld') ',' num2str(end_ts, '%ld') ',' vid ',' model_name ',' values]);
	fprintf(fid,'%s\n', outstr);
	fclose(fid);
endfunction