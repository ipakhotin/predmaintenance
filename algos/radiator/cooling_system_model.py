import pandas as pd
import sys
import numpy as np
from pathlib import Path
import os 

def speedclassification(row):
    if int(row['Speed']) == 0:
        val = 'Zero'
    elif int(row['Speed']) < 20:
        val = 'XSlow'
    elif (int(row['Speed']) >= 20) and (int(row[1]) < 50):
        val = 'Slow'
    elif int(row['Speed']) >= 50 and int(row[1]) <= 80:
        val = 'Average'
    elif int(row['Speed']) > 80:
        val = 'Fast'
    return val
    pass

def readObdIISpeed(vehicle_id, files, outputPath):
    """
    
    """
    timestart = 0
    nextDay = 24*60*60*1000
    count = 0
    cwd = os.getcwd()
    
    files[0] = cwd + '/' + files[0]
    files[1] = cwd + '/' + files[1]
    files[2] = cwd + '/' + files[2]
    print(cwd)
    
    speedData = pd.read_csv(files[0], usecols = [1,4]).rename(columns={'t': 't', 'Value': 'Speed'})
    tempData = pd.read_csv(files[1], usecols = [1,4]).rename(columns={'t': 't', 'Value': 'Temp'})
    rpmData = pd.read_csv(files[2], usecols = [1,4]).rename(columns={'t': 't', 'Value': 'RPM'})
   
    
    if len(speedData) != len(tempData) != len(rpmData):
        print ("Records are not same size")
        sys.exit(0)
    
    print("All three files records matched, Starting processing")
    
    speedData = pd.merge(speedData,tempData,on='t').merge(rpmData,on='t')
    speedData['Quality'] = speedData.apply(speedclassification,axis=1)
    
    print("Data Formatting Started")
    #print(speedData)
    timestart = speedData['t'].min()
   
    while(speedData['t'].max() >= timestart):
        result = pd.DataFrame(data=[['Zero',0],['XSlow',0],['Slow',0],['Average',0],['Fast',0]],index=[0,1,2,3,4],columns=['Quality','Value']).merge(
        speedData[(speedData['t'] > timestart) & (speedData['t'] <= (timestart+nextDay)) & (speedData['RPM'] > 400)].groupby(by='Quality').mean().reset_index(),on='Quality',how='outer').fillna('null')
        timestart += nextDay
        
        fd = open(outputPath+str(vehicle_id)+'_output.csv','a+')
        fd.write(str(timestart) + ", ")
        fd.write(", ".join(str(e) for e in list(result['Temp'])) + "\n")
        fd.close()
        
        count += 1
        print('Day:{}'.format(count))
        
    sys.exit()
    
    #Old way
    #bracket = [[1,1],[1,1],[1,1],[1,1],[1,1]]
    #while(len(speedData) > count):
        
    #    current_speed = list(speedData.iloc[count])
    #    current_temp = list(tempData.iloc[count])
    #    current_rpm = list(rpmData.iloc[count])
        
        
    #    if current_speed[0] == current_temp[0] == int(current_rpm[0]):
    #        if int(current_speed[0]) >= (timestart + nextDay):
    #            timestart += nextDay
    #            numberOfDays += 1
    #            
    #            myCsvRow = [item[0]/item[1] for item in bracket]
    #            fd = open('output.csv','a+')
    #            fd.write(", ".join(str(e) for e in myCsvRow) + "\n")
    #            fd.close()
                
    #            bracket = [[1,1],[1,1],[1,1],[1,1],[1,1]]
    #            print("Number of Days:{}".format(numberOfDays))
            
    #        if int(current_rpm[1]) < 400:
    #            print("Engine is off and speed is {} and rpm is {}".format(current_speed[1], current_rpm[1]))
    #            count += 1
    #            continue
             
    #        if int(current_speed[1]) == 0:
    #            bracket[0][1] += 1
    #            bracket[0][0] += int(current_temp[1])
    #        elif int(current_speed[1]) < 20:
    #            bracket[1][1] += 1
    #            bracket[1][0] += int(current_temp[1])
    #        elif (int(current_speed[1]) >= 20) and (int(current_speed[1]) < 50):
    #            bracket[2][1] += 1
    #            bracket[2][0] += int(current_temp[1])
    #        elif int(current_speed[1]) >= 50 and int(current_speed[1]) < 80:
    #            bracket[3][1] += 1
    #            bracket[3][0] += int(current_temp[1])
    #        elif int(current_speed[1]) > 80:
    #            bracket[4][1] += 1
    #            bracket[4][0] += int(current_temp[1])
                
    #    count += 1
        
    pass


if len(sys.argv) <= 2:
    print("Usage: SpeedFile,TempFile,RPMFile")
    sys.exit(0)

vehicle_id = int(sys.argv[1])
files = sys.argv[2].split(',')

files[0] = files[0].split('[')[1]
files[2] = files[2].split(']')[0]
files[0] = files[0].strip("/").strip("'")
files[1] = files[1].strip("/").strip(" ").strip("'")
files[2] = files[2].strip("/").strip(" ").strip("'")

str_path = "output/"
path = Path(str_path)
path.mkdir(parents=True, exist_ok=True)

str_path = "output/radiator/"
path = Path(str_path)
path.mkdir(parents=True, exist_ok=True)

readObdIISpeed(vehicle_id, files, str_path)
