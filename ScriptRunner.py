from Const import Constants
from glob import glob
import os
import subprocess
import csv
from OutputFileConversion import OutputFileConversion

class ScriptRunner:
    
    def runScript(self, scriptFile, formatedDict, lstChannel, lstVehicles, parentpath, type):
        #see the directory to look into
        parentpath = parentpath + "Combined/"
        directorypath = ""
        
        #look for start time and end time in the query
        start_time = ""
        end_time = ""
        for key, value in formatedDict.items():
            if key == Constants.CONST_T:
                start_time = value[0]
            
            if key == Constants.CONST_ET:
                key = Constants.CONST_T
                end_time = value[0]
        vehicle_records = []
        
        if len(lstVehicles) == 0:
            for channels in lstChannel:
                temppath = parentpath + channels
                vehicle_records.extend(glob(temppath+'/' +'*'))
        
            parsed_vehicles = [os.path.basename(file).split('_')[0] for file in vehicle_records]
            lstVehicles = set(parsed_vehicles)
            
        if(len(lstVehicles) == 0):
            print("There is no data available in cache, please run the db query first")
        
        #directory path and times are in place, to look for data, find the appropriate files to process
        files = []
        
        for vehicle in lstVehicles:
            files = []
            for channels in lstChannel:
                directorypath = parentpath + channels
                if not os.path.isdir(directorypath):
                    print(directorypath)
                    print("One or more of the directories doesn't exist, Please conntact IT department")
                
                
                tempFiles = glob(directorypath+ '/' + vehicle+'*')
                
                if len(tempFiles) == 0:
                    files = []
                    break
                
               
                tempFiles = self.checkCondition(tempFiles, lstChannel, start_time, end_time, directorypath)
                
                if len(tempFiles) == 0:
                    files = []
                    break
                
                files.extend(tempFiles)        

            
            if type == 1:
                if len(files) > 0:
                    print(files)
                    print ("--Running " + scriptFile + " with file:" + files[0])
                    print(subprocess.run(["octave", "--quiet", scriptFile, vehicle, str(files)], stdout=subprocess.PIPE, stderr=subprocess.PIPE))
                    print ("--Finished " + scriptFile + " with file:" + files[0])
            else:
                if len(files) > 0:
                    print(files)
                    print ("--Running " + scriptFile + " with file:" + files[0])
                    print(subprocess.run(["python3", scriptFile, vehicle, str(files)], stdout=subprocess.PIPE))
                    print ("--Finished " + scriptFile + " with file:" + files[0])
                
    
    def checkCondition(self, files, lstChannel, start_time, end_time, directorypath):
        processed_files = []
        vehicleIds = ""
        combinedChannels = []
        for csvFiles in files:
                
            parsedFileName = os.path.splitext(os.path.basename(csvFiles))[0]
            nameParts = parsedFileName.split("_")
            
            #put the name in assigned variables
            vehicleIds = nameParts[0]
            combinedChannels = [nameParts[o] for o in range(1,1 + 1)]
            
            file_start_time = ''
            file_end_time = ''
            
            #First index is vehicle id, only one channel should come now
            if len(nameParts) > (1+ 1):
                file_start_time = nameParts[1+1]
            
            #First index is vehicle id, plus start date need to filtered for end date, only one channel should come now    
            if len(nameParts) > (2+ 1):
                file_end_time = nameParts[2+1]
                
            timeConstraintPassed = False
            #Filter files on the basis of start date and end date
            if not start_time and not end_time:
                timeConstraintPassed = True
            elif not end_time:
                if start_time > file_start_time:
                    timeConstraintPassed = True
            else:
                if start_time <= file_start_time and end_time >= file_end_time:
                    timeConstraintPassed = True
            
            #Check if the time constraint is passed
            if not timeConstraintPassed:
                continue
            
            processed_files.append(csvFiles)
        
        #more than one file, need to merge
        newFileName = ""
        
        sorted_list = []
        if len(processed_files) > 1:
        
            newFileName = vehicleIds
               
            for allChannels in combinedChannels:
                newFileName = newFileName + "_" + allChannels
            newFileName = newFileName + "_" + start_time + "_" + end_time + "_S"
            
            newFileName = directorypath + "/" + newFileName
            
            for csvFiles in processed_files:
                reader = csv.reader(open(csvFiles), delimiter=",")
                header = next(reader)
                sorted_list.extend(sorted(reader, key=lambda row: row[1], reverse=False))
            
            results = sorted(sorted_list, key=lambda row: row[1], reverse=False)
            
            
            description = [['c'],['t'],['m'],['v']]
            datetime = ['DateTime']
            channelName = ['ChannelName']
            channelValue = ['Value']
            dataType = ['Type']
        
            convert = OutputFileConversion("csv")
            newFileName += ".csv"
            with open(newFileName, 'w') as csvfile:
                desc = (description[0],description[1],description[2],description[3],channelValue, datetime, channelName, dataType)
                fieldnames = [description[0][0],description[1][0],description[2][0],description[3][0],channelValue[0], datetime[0], channelName[0], dataType[0]]
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

                writer.writeheader()
                #print (list(self.convertDBToJson(description,vehicle_result)))
                writer.writerows (list(convert.convertDBToJson(desc, results)))
            processed_files = []
            processed_files.append(newFileName)
        
        return processed_files
            
