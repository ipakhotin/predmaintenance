from Const import Constants
from glob import glob
from OutputFileConversion import OutputFileConversion
import csv
import os
import fnmatch
import re
from pathlib import Path
import shutil




class ChannelsCombiner:
    
    def ReadVehicleFilesAndWrite(self, vehicle, files, filename, foldername, start_time, end_time, parentpath, dir):
        files = []
        filename = vehicle
        absolutePath = parentpath + dir
        files.extend(glob(absolutePath+'/' + vehicle +'*'))
        self.WriteFiles(files, filename, foldername, start_time, end_time, parentpath, dir, vehicle)
    
    #Combine the channels or put in CWD
    def Combine(self, dict, channels, vehicles, parentpath, combineVehicles):
        str_path = parentpath + "Combined/"
        if os.path.isdir(str_path):
            shutil.rmtree(str_path, ignore_errors=True)
        start_time = ""
        end_time = ""
        for key, value in dict.items():
            if key == Constants.CONST_T:
                start_time = value[0]
            
            if key == Constants.CONST_ET:
                key = Constants.CONST_T
                end_time = value[0]

        values = [o for o in os.listdir(parentpath) if os.path.isdir(os.path.join(parentpath,o))]
        filename = ""
        foldername = ""
        files = []
        
        for dir in values:
            #Directory is found, move forward
            if dir in channels:
                #if require to combine vehicles data
                files = []
                absolutePath = parentpath + dir
                #Case if data is required for specific vehicles
                if len(vehicles) > 0:
                    filename = ""
                
                    if combineVehicles is True:
                        for vehicle in vehicles:
                            if not filename:
                                filename = vehicle
                            else:
                                filename = filename + '-' + vehicle
                            files.extend(glob(absolutePath+'/' + vehicle +'*'))
                        self.WriteFiles(files, filename, foldername, start_time, end_time, parentpath, dir, vehicle)
                    else:
                        for vehicle in vehicles:
                            self.ReadVehicleFilesAndWrite(vehicle, files, filename, foldername, start_time, end_time, parentpath, dir)
                #Case if data is required for all vehicles
                else:
                    absolutePath = parentpath + dir
                    files.extend(glob(absolutePath+'/' +'*'))
                    
                    parsed_vehicles = [os.path.basename(file).split('_')[0] for file in files]
                    parsed_vehicles = set(parsed_vehicles)
                    for all_vehicles in parsed_vehicles:
                        self.ReadVehicleFilesAndWrite(all_vehicles, files, filename, foldername, start_time, end_time, parentpath, dir)
                    
                    
        return 0
    
    
    def WriteFiles(self, files, filename, foldername, start_time, end_time, parentpath, channels, vehicles):
        print (files)
        str_path = parentpath + "Combined/"
        path = Path(str_path)
        path.mkdir(parents=True, exist_ok=True)
        
        if(len(files) == 0):
            print("Sorry no data available for channel "+ str(channels)+" for vehicles " + str(vehicles) + " at this moment")
            return
        sortedlist = []
        
        filename = filename+"_"+channels
        if not foldername:
            foldername = channels
        else:
            foldername = foldername+ "_" + channels
        
        
        str_path = str_path + foldername + "/"
        path = Path(str_path)
        path.mkdir(parents=True, exist_ok=True)
                
        
        if start_time and end_time:
            filename = filename+ "_" +str(start_time)
            filename = filename+ "_" +str(end_time)
            
        elif start_time:
            filename = filename+ "_" +start_time
            
       
        for csvFiles in files:
            if not start_time and not end_time:
                reader = csv.reader(open(csvFiles), delimiter=",")
                sortedlist.extend(sorted(reader, key=lambda row: row[1], reverse=False))
            elif not end_time:
                parsedFileName = os.path.splitext(os.path.basename(csvFiles))[0]
                if (int(start_time) <= int(splitedText[1]) or int(start_time) >= int(splitedText[2])):
                    reader = csv.reader(open(csvFiles), delimiter=",")
                    sortedlist.extend(sorted(reader, key=lambda row: row[1], reverse=False))
            else:
                parsedFileName = os.path.splitext(os.path.basename(csvFiles))[0]
                splitedText = parsedFileName.split('_')
                if (int(start_time) <= int(splitedText[1]) or int(start_time) >= int(splitedText[2])) and (int(end_time) >= int(splitedText[2])):
                    reader = csv.reader(open(csvFiles), delimiter=",")
                    header = next(reader)
                    sortedlist.extend(sorted(reader, key=lambda row: row[1], reverse=False))
        
        filename = str_path + filename + ".csv"
       
        
        results = sorted(sortedlist, key=lambda row: row[1], reverse=False)
        if len(results) <= 0:
            print("Sorry no records found")
            return;
        #print(results)
        description = [['c'],['t'],['m'],['v']]
        datetime = ['DateTime']
        channelName = ['ChannelName']
        channelValue = ['Value']
        dataType = ['Type']
        
        convert = OutputFileConversion("csv")
        
        with open(filename, 'w') as csvfile:
            desc = (description[0],description[1],description[2],description[3],channelValue, datetime, channelName, dataType)
            fieldnames = [description[0][0],description[1][0],description[2][0],description[3][0],channelValue[0], datetime[0], channelName[0], dataType[0]]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()
            #print (list(self.convertDBToJson(description,vehicle_result)))
            writer.writerows (list(convert.convertDBToJson(desc, results)))
        
