import Enumerations
import csv
import datetime as DT
from pathlib import Path
from Const import Constants

class OutputFileConversion:
    'Class to parse the input type to given outputs'
    
    def __init__(self, outputType):
        self.type = outputType
    
    def convertDBOutput(self, description, results, start_time, end_time, parent_path, channels):
        if self.type ==  Enumerations.OutputType.JSON:
            return self.convertDBToJson(description, results)
        elif self.type == Enumerations.OutputType.CSV:
            return self.convertDBToCSV(description, results, start_time, end_time, parent_path, channels)
        
    #convert db reply to json
    def convertDBToJson(self, description, results):
        cols = [c[0] for c in description]
        return [dict(zip(cols, r)) for r in results]
    
    
    #convert db reply to csv
    def convertDBToCSV(self, description, results, start_time, end_time, parent_path, channels):
        cols = [c[0] for c in description]
        vehicleslist = [r[3] for r in results]
        vehicles = set(vehicleslist)
        #Actually 4th row contains first element but in order to keep consistency put 3 here
        count = 4
        #create the directory if doesn't exists

        for key,channel in channels.items():
            str_path = parent_path+channel[0] + "/"
            
            path = Path(str_path)
            path.mkdir(parents=True, exist_ok=True)
            
            
            for vehicle in vehicles:
                vehicle_result = [i for i in results if i[3] == vehicle]
                
                final_results = []
                #print(vehicle_result)
                
                subchannelcount = 0
                for each_result in vehicle_result:
                    resultcolumn = ""
                    
                    if channel[0] in Constants.SUBCHANNELS:
                        subchannelcount = 0
                        for subChannel in Constants.SUBCHANNELS[channel[0]]:
                            if not resultcolumn:
                                resultcolumn = str(each_result[count + subchannelcount])
                                subchannelcount += 1
                            else:
                                resultcolumn = str(resultcolumn) + "," + str(each_result[count + subchannelcount])
                                subchannelcount += 1
                    else:
                        resultcolumn = each_result[count]
                    
                    time = DT.datetime.fromtimestamp(int(each_result[1])/1000).strftime('%Y-%m-%d %H:%M:%S.%f')
                    filtered_result = [each_result[0], each_result[1], each_result[2], each_result[3], resultcolumn, time, channel[0], Constants.CHANNELTYPES[channel[0].upper()]]
                    final_results.append(filtered_result)
                
    
                if subchannelcount == 0:
                    count += 1
                else:
                    count += subchannelcount
                #final_results = [vehicle_result[0],vehicle_result[1],vehicle_result[2],vehicle_result[3],vehicle_result[count]]
    
                filename = ""
                if start_time != 0 and end_time != 0:
                    filename = str(vehicle) + "_" + str(start_time) + "_" + str(end_time)
                elif start_time != 0:
                    filename = str(vehicle) + "_" + str(start_time)
                else:
                    filename = str(vehicle)
                filename += ".csv"
                
                print (str_path +filename)
                datetime = ['DateTime']
                channelName = ['ChannelName']
                channelValue = ['Value']
                dataType = ['Type']
                with open(str_path +filename, 'w') as csvfile:
                    desc = (description[0],description[1],description[2],description[3],channelValue, datetime, channelName, dataType)
                    fieldnames = [cols[0],cols[1],cols[2],cols[3],channelValue[0], datetime[0], channelName[0], dataType[0]]
                    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

                    writer.writeheader()
                    #print (list(self.convertDBToJson(description,vehicle_result)))
                    writer.writerows (list(self.convertDBToJson(desc, final_results)))
 
        return