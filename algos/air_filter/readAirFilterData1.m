
%get MAF data
%[a1,a2] = textread('car219OBDII_MAFon01-01-2017.txt',' %f %f','delimiter',',');
[a1,a2] = textread('car219OBDII_MAFon01-01-2017.txt',' %f %f','delimiter',',');

tsmaf = a1;
maf = a2;
clear a1 a2

%get TEMP data
%[a1,a2] = textread('car219OBDII_TEMPon01-01-2017.txt',' %f %f','delimiter',',');
[a1,a2] = textread('car219OBDII_TPSon01-01-2017.txt',' %f %f','delimiter',',');

tstps = a1;
tps = a2;
clear a1 a2

%get SPEED data
%[a1,a2] = textread('car219OBDII_RPMon01-01-2017.txt',' %f %f','delimiter',',');
[a1,a2] = textread('car219OBDII_SPEEDon01-01-2017.txt',' %f %f','delimiter',',');

tsspeed = a1;
speed = a2;
clear a1 a2

%get RPM data
%[a1,a2] = textread('car219OBDII_RPMon01-01-2017.txt',' %f %f','delimiter',',');
[a1,a2] = textread('car219OBDII_RPMon01-01-2017.txt',' %f %f','delimiter',',');

tsrpm = a1;
rpm = a2;
clear a1 a2

%get ACCEL data

[a1,a2] = textread('car219IMU_ACCEL_Xon01-01-2017.txt',' %f %f','delimiter',',');

tsaccelx = a1;
accelx = a2;
clear a1 a2

[a1,a2] = textread('car219IMU_ACCEL_Yon01-01-2017.txt',' %f %f','delimiter',',');

tsaccely = a1;
accely = a2;
clear a1 a2

[a1,a2] = textread('car219IMU_ACCEL_Zon01-01-2017.txt',' %f %f','delimiter',',');

tsaccelz = a1;
accelz = a2;
clear a1 a2
%}
%plot data
%{
figure
plot((tsmaf-tsmaf(1))/1000,maf)
hold on
plot((tstps-tsmaf(1))/1000,tps,'r')
plot((tsrpm-tsmaf(1))/1000,rpm/500,'k')
plot((tsspeed-tsmaf(1))/1000,speed,'m')
%}