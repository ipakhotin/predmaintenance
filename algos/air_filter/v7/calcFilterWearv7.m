
%get data
%readAllData2


addpath "algos"
vid = argv(){1};
read_data_to_arrays;


[tsmaf, maf] = getMAF(vid);
[tsrpm, rpm] = getRPM(vid);
[tsspeed, speed] = getSpeed(vid);
[tstps, tps] = getTPS(vid);

%get rid of any TPS=0 values. TPS should never be zero!

tps(tps==0) = NaN;
rpm(isnan(tps)==1) = NaN;
maf(isnan(tps)==1) = NaN;
speed(isnan(tps)==1) = NaN;

tps(isnan(tps)==1) = [];
rpm(isnan(rpm)==1) = [];
maf(isnan(maf)==1) = [];
speed(isnan(speed)==1) = [];

%calc TPS floor periods
if(tps(1)==min(tps))
firstindex = 1;
else
tmp = find(tps==min(tps));
firstindex = tmp(1);
clear tmp
end

if(tps(end)==min(tps))
lastindex = length(tps);
else
tmp = find(tps==min(tps));
lastindex = tmp(end);
clear tmp
end

beginidx = NaN;endidx = NaN;

for i=firstindex:lastindex
if(i>1 && tps(i)>min(tps) && tps(i-1)==min(tps))
beginidx(end+1) = i;
end

if(i<lastindex && tps(i)>min(tps) && tps(i+1)==min(tps))
endidx(end+1) = i;
end

end

beginidx(1) = [];endidx(1) = [];

%POSSIBLE IMPROVEMENT - discard really long throttle periods (since they will presumably be more noisy)

maxLength = 10;

for i=1:length(beginidx)
if((endidx(i)-beginidx(i))>maxLength)
beginidx(i) = NaN;endidx(i) = NaN;
end
end

beginidx(isnan(beginidx)==1) = [];
endidx(isnan(endidx)==1) = [];

%discart short throttle periods (to exclude singleton events)

minLength = 2;

for i=1:length(beginidx)
if((endidx(i)-beginidx(i))<minLength)
beginidx(i) = NaN;endidx(i) = NaN;
end
end

beginidx(isnan(beginidx)==1) = [];
endidx(isnan(endidx)==1) = [];
%}

%then see the delta v for that throttle cycle

for i=1:length(beginidx)
sumtps(i) = sum(tps(beginidx(i):endidx(i)));
meantps(i) = mean(tps(beginidx(i):endidx(i)));
mediantps(i) = median(tps(beginidx(i):endidx(i)));

meanrpm(i) = mean(rpm(beginidx(i):endidx(i)));
medianrpm(i) = median(rpm(beginidx(i):endidx(i)));
sumrpm(i) = sum(rpm(beginidx(i):endidx(i)));
deltarpm(i) = rpm(endidx(i))-rpm(beginidx(i));

summaf(i) = sum(maf(beginidx(i):endidx(i)));
meanmaf(i) = mean(maf(beginidx(i):endidx(i)));
medianmaf(i) = median(maf(beginidx(i):endidx(i)));

deltav(i) = speed(endidx(i))-speed(beginidx(i));
end

%sort by sum TPS to get the maxima

clear meantpsarray idx idx2 idx3 maxtps maxdeltav
meantpsarrayinterval = 1;
meantpsarray = [0:meantpsarrayinterval:60];

for i=1:length(meantpsarray)
idx1 = find(meantps>(meantpsarray(i)-meantpsarrayinterval/2));
idx2 = find(meantps<(meantpsarray(i)+meantpsarrayinterval/2));
idx3 = intersect(idx1,idx2);
if(isempty(idx3)==0)
maxtps(i) = max(meantps(idx3));
maxdeltav(i) = max(deltav(idx3));
meanmeanrpm(i) = mean(meanrpm(idx3));
medianmeanrpm(i) = median(meanrpm(idx3));
maxmeanrpm(i) = max(meanrpm(idx3));
else
maxtps(i) = NaN;
maxdeltav(i) = NaN;
meanmeanrpm(i) = NaN;
medianmeanrpm(i) = NaN;
maxmeanrpm(i) = NaN;
end
clear idx idx2 idx3
end

%sort by sum TPS to get the maxima

clear sumtpsarray maxdeltarpm idx idx2 idx3 maxtps maxdeltav uqdeltarpm
sumtpsarrayinterval = 10;
sumtpsarray = [0:sumtpsarrayinterval:600];

for i=1:length(sumtpsarray)
idx1 = find(sumtps>(sumtpsarray(i)-sumtpsarrayinterval/2));
idx2 = find(sumtps<(sumtpsarray(i)+sumtpsarrayinterval/2));
idx3 = intersect(idx1,idx2);
if(isempty(idx3)==0)
maxtps(i) = max(sumtps(idx3));
maxdeltav(i) = max(deltav(idx3));
maxdeltarpm(i) = max(deltarpm(idx3));
arraytmp = deltarpm(idx3)';
ytmp = quantile(arraytmp,[0.10 0.5 0.90]);
uqdeltarpm(i) = ytmp(end);
clear arraytmp ytmp
else
maxtps(i) = NaN;
maxdeltav(i) = NaN;
maxdeltarpm(i) = NaN;
uqdeltarpm(i) = NaN;
end
clear idx idx2 idx3
end

idx = find(sumtpsarray>120);
curve = uqdeltarpm(1:idx(1));
sumOut = nansum(curve)
clear idx

%output to a file

filename = 'airFilterOutput.txt';
fid = fopen(filename,'a');
outstr = [num2str(sumOut)];
fprintf(fid,"%s\n",outstr);
fclose(fid);

%run ordinary least squares linear regression for the well-behaved part of the scatter plot
%{
clear maxtps2 maxdeltav2 Xt Yt x N K bet ut

idx = find(maxtps>1500);

maxtps2 = maxtps(1:idx(1));
maxdeltav2 = maxdeltav(1:idx(1));
clear idx

maxtps2(isnan(maxtps2)==1) = [];
maxdeltav2(isnan(maxdeltav2)==1) = [];

Xt = maxtps2;
Yt = maxdeltav2;

X = [ones(length(Xt),1), Xt'];
[N,K]=size(X);
bet = X\Yt(:);
ut = Yt' - X*bet;

%calc mean value for output

meanval = mean(X*bet);

%output to a file

filename = 'airFilterOutput.txt';
fid = fopen(filename,'a');
outstr = [num2str(meanval)];
fprintf(fid,"%s\n",outstr);
fclose(fid);
%}
