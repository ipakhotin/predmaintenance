from enum import IntEnum
    
class InputType(IntEnum):
    DB = 1
    Clickdrive = 2
    CAN = 3
        
class OutputType(IntEnum):
    CSV = 1
    JSON = 2
    
class Conditions(IntEnum):
    EQUAL = 1
    NOTEQUAL = 2
    GREATER = 3
    LESSER = 4
    

