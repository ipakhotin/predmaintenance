
%two time periods - both DDMMYYYY
date1 = '01012017';
date2 = '01062017';

%need TPS, MAF, SPEED, ACCEL.X, ACCEL.Y, ACCEL.Z
%each input file should have 2 columns: timestamp and the parameter (e.g. speed, MAF etc)

%read air filter data
%readAirFilterData1

addpath "algos"
vid = argv(){1};
%vid = 241;

read_data_to_arrays;

[ts_maf, maf] = getMAF(vid);
[ts_rpm, rpm] = getRPM(vid);
[ts_speed, speed] = getSpeed(vid);
[ts_tps, tps] = getTPS(vid);

throttleMAFratio = (rpm.*tps)./maf;

%calc acceleration

spd = 0;

for i=2:length(speed)

if(speed(i)>0 && (speed(i)-speed(i-1))==0)
spd(i) = NaN;
else
spd(i) = speed(i);
end
end

idx = find(isnan(spd)==0);

for i=2:length(idx)
segment = spd(idx(i-1):idx(i));
lengthOfSegment = length(segment);
if(lengthOfSegment>1)
interpSegment = interp1([1 lengthOfSegment],[segment(1) segment(end)],[1:1:lengthOfSegment]);
spd(idx(i-1):idx(i)) = interpSegment;
end
clear segment lengthOfSegment interpSegment

end

accn(1) = 0;
%{
for i=2:length(spd)
accn(i) = (((spd(i)-spd(i-1))/3.6)/((tsspeed(i)-tsspeed(i-1))/1000))/9.8;
end
%}

interval = 2;

for i=interval+1:length(spd)
accn(i) = (((spd(i)-spd(i-interval))/3.6)/((ts_speed(i)-ts_speed(i-interval))/1000))/9.8;
end

%find periods of fixed forward acceleration - this method assumes the car mass is roughly constant 

idx1 = find(accn>0.1);
idx2 = find(accn<0.2);
idx = intersect(idx1,idx2);

throttleAtThatAccel = tps(idx);
mafAtThatAccel = maf(idx);

%apply same methodology to another time period

%I don't have a suitable time period yet so will manufacture an example

%throttleAtThatAccel2 = throttleAtThatAccel*1.2;
%mafAtThatAccel2 = mafAtThatAccel;

%calc ratio

%throttleRatio = median(throttleAtThatAccel2)/median(throttleAtThatAccel);
%mafRatio = median(mafAtThatAccel2)/median(mafAtThatAccel);

%output to file
format_output;

write(ts_tps(1), ts_tps(end), vid, 'air_filter', num2str(meanval));
